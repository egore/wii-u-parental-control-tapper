About
=====

The Nintendo Wii U can be locked with a 4 digit code. If the knowledge about
the code was lost and also the logged in Nintendo account is password
protected, the Wii U can no longer be used.

To me no method is known to circumvent this via a software or hardware based
approach. This repository provides an Arduino Nano V3 based approach for brute
forcing the parental PIN.

Approach
========

To brute force the parental PIN you have to type the values from 0000 to 9999.
Luckily there is no delaying or even permanent locking of the console, so doing
as many tries as necessary is possible.

The necessary hardware for this project:

 - Arduino Nano V3 with 12 digital PINs (D2 to D13) https://www.ebay.de/itm/162474269232
 - Mini Breadboard with jumper cables https://www.ebay.de/itm/143991989300
 - 12 Micro Servo Motors https://www.amazon.de/gp/product/B07MPPF5CS
 - 12 Stylus Stift Touch Pen https://www.amazon.de/gp/product/B074C6X1LY
 - a 3D printed 

The main code is in touchpad/touchpad.ino. It is losely based on
https://draeger-it.blog/arduino-lektion-16-servo-ansteuern/. In the setup()
part it will set the motors to a zero position and in each loop() it will type
one of the PINs. Every three loops it has to restart the process on the Wii U,
as the device only allows three attempts and redirects to the start screen
then.

Disclaimer
==========

Nintendo and Wii U are trademarks of Nintendo Co., Ltd. The author of this code
does not claim any copyright on these brand names.

The code is provided as is for educational purposes. You are not allowed to
sell it in any form without the authors permission.