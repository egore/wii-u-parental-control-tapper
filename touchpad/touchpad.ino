#include <Servo.h>

/**
 * Numer of servo motors attached to the Arduino Nano v3.
 */
const int MAX_PADS = 10;
/**
 * Duration in milliseconds to touch the surface
 */
const int TOUCH_TIME = 50;

/**
 * Due to the space constraints some motors have to tap from 0° to 20°, others have to tap
 * from 20° to 0°. When a motor is set to true here, it will tap from 20° to 0°.
 *
 * IMPORTANT: Please ensure the motors are reset once before attaching the arms, otherwise
 * the arms will interfere with its surroundings an possibly break.
 */
bool INVERSE[] = {
  true,  /* 0 */
  false, /* 1 */
  false, /* 2 */
  false, /* 3 */
  true,  /* 4 */
  false, /* 5 */
  true,  /* 6 */
  false, /* 7 */
  false, /* 8 */
  true   /* 9 */
};
/**
 * Array of attached servos. The index matches the pad to be pressed, i.e. servo[0] is the
 * numer "0" on the Wii U touchpad.
 */
Servo digits[MAX_PADS];
/**
 * Servo to press the OK button
 */
Servo ok;
/**
 * Servo to press the start button
 */
Servo start;

/**
 * Buffer for the taps to perform in this iteration
 */
int buffer[4];

int currentPin = 0;

void setup() {

  // Attach all the servo motors and reset their position to "not touching"
  for (int i = 0; i < MAX_PADS; i++) {
    digits[i].attach(i + 2);
    digits[i].write(INVERSE[i] ? 20 : 0);
    // Delay by 100ms to not drain to much power driving all motors in parallel
    delay(100);
  }

  // Attach and reset OK button
  ok.attach(12);
  ok.write(0);
  delay(100);

  // Attach and reset Start button
  start.attach(13);
  start.write(20);

  // Finally wait 500ms before we start, so the Arduino and the servos can settle
  delay(500);
}

/**
 * Tap a specific button
 * 
 * @param index the button to press (e.g. passing 3 will press number 3 on the touchpad)
 */
void tap(int index) {
  tap(digits[index], INVERSE[index]);
}

void tap(Servo& servo, bool inverse) {
  // Put the finger down for 50ms
  servo.write(inverse ? 0 : 20);
  delay(TOUCH_TIME);
  servo.write(inverse ? 20 : 0);
}

void fillBufferWithDigits() {
  // Store the current PIN locally, so we can play with its digits
  int localPin = currentPin;
  for (int i = 3; i >= 0; i--) {
    int reallyLocal = localPin % 10;
    buffer[i] = reallyLocal;
    localPin -= reallyLocal;
    localPin = round(localPin / 10);
  }
}

// One PIN tested per loop
void loop() {

  // Every three iteration we need to re-enter the PIN screen
  if (currentPin % 3 == 0) {
    tap(start, false);
    delay(400);
  }

  fillBufferWithDigits();
  
  for (int i = 0; i < 4; i++) {
    tap(buffer[i]);
    // Wait 200ms until the next tap
    delay(200);
  }
  delay(300);
  tap(ok, false);

  // Wait until the PIN was checked
  delay(500);
  // Assume it failed when the OK button appears right over the 8 button
  tap(8);

  // Wait two seconds before we start the next round
  delay(2000);

  // Go to the next PIN, or start over if we reached the end
  currentPin++;
  if (currentPin > 9999) {
    currentPin = 0;
  }
}
